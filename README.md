# Playing with GStreamer
# Build Gstreamer Playground
```sh
docker build . -t gstreamer-playground 
```
# Start the playground
```sh
docker run --name gstp -dit --rm -p 5000:5000/udp --network host gstreamer-playground
```

# Connect to the playground container
```sh
docker exec -it gstp bash
```
