#!/bin/bash
gst-client pipeline_create simple_udp_server \
    videotestsrc \
    ! videoconvert \
    ! x264enc \
    ! rtph264pay \
    config-interval=1 \
        pt=96 \
    ! udpsink \
        host=host.docker.internal \
        port=5000

gst-client pipeline_play simple_udp_server
