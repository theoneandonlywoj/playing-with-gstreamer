#Download base image ubuntu 18.04
FROM ubuntu:18.04

LABEL maintainer="theoneandonlywoj@gmail.com"
LABEL version="0.1"

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Updating the package list
RUN apt update

# Basic tools
RUN apt-get install -y \
        git \
        gtk-doc-tools

# GStreamer
RUN apt-get install -y \
        libx264-dev \
        libjpeg-dev
RUN apt-get install -y \
        libgstreamer1.0-dev \
        libgstreamer-plugins-base1.0-dev \
        libgstreamer-plugins-bad1.0-dev \
        gstreamer1.0-plugins-ugly \
        gstreamer1.0-plugins-good \
        gstreamer1.0-plugins-bad \
        gstreamer1.0-tools

# Gst-Daemon build dependencies
RUN apt-get install -y \
        automake \
        libtool \
        pkg-config \
        libgstreamer1.0-dev \
        libgstreamer-plugins-base1.0-dev \
        libglib2.0-dev \
        libjson-glib-dev \
        gtk-doc-tools \
        libreadline-dev \
        libncursesw5-dev \
        libdaemon-dev \
        libjansson-dev \
        libsoup2.4-dev \
        python3-pip \
        libedit-dev \
        python3 \
        python3-setuptools \
        python3-wheel \
        ninja-build

# Meson
RUN pip3 install meson

# Gst-Daemon
RUN git clone https://github.com/RidgeRun/gstd-1.x.git gst-daemon-git
WORKDIR "gst-daemon-git"
RUN meson build
RUN ninja -C build install

# Come back to the main folder
WORKDIR "/"

# Gst Interpipe
RUN git clone https://github.com/RidgeRun/gst-interpipe.git gst-interpipe-git
WORKDIR "/gst-interpipe-git"
RUN mkdir build
RUN meson build --prefix=/usr
RUN ninja -C build
RUN ninja -C build install

# Come back to the main folder
WORKDIR "/"

# Copy the scripts
ADD examples/scripts_docker /scripts/

EXPOSE 5000/udp
# Runing Gst-Daemon as a background process
CMD gstd -e && tail -f /dev/null
