# http://byterot.blogspot.com/2022/04/httpsbyterot.blogspot.com202204sending-udp-docker-container-localhost.html

#!/bin/bash
gst-launch-1.0 -v \
	videotestsrc \
	! videoconvert \
	! x264enc \
	! rtph264pay \
		config-interval=1 \
		pt=96 \
	! udpsink \
		host=host.docker.internal \
		port=5000
